package ru.pae.task6;

import java.util.Scanner;

public class Task6 { //Вывод таблицы умножения введённого числа
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите число, которое хотите умножить - ");
        int number = sc.nextInt();

        for (int i = 1; i < 10; i++) {
            System.out.println(number + " * " + i + " = " + (number*i));
        }
    }
}