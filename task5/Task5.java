package ru.pae.task5;

public class Task5 {
    public static void main(String[] args) {
        int x, y;
        for (x = 2; x < 101; x++) {
            y = 0;
            for (int i = 2; i <= x; i++) {
                if (x % i == 0)
                    y++;
            }
            if (y <= 2)
                System.out.println(x + " простое число");
        }
    }
}