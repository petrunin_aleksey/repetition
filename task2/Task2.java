package ru.pae.task2;

public class Task2 {
    public static void main(String[] args) {
        double[] arr = new double[5];

        arr[0] = 120;
        arr[1] = 67;
        arr[2] = 20;
        arr[3] = 0;
        arr[4] = 110;

        for (int num = 0; num < arr.length; num++) {
            arr[num] = (arr[num] * 1.1);
            System.out.println(arr[num]);
        }
    }
}
